package cest.edu.aval2.principal;

import java.util.ArrayList;

import cest.edu.aval2.empresa.Atendimento;

public class Cliente extends PessoaFisica {
	ArrayList<Atendimento> Atendimentos;
	
	@Override
	public String getNome() {
		return super.getNome();
	}

	@Override
	public String getCPF() {
		return super.getCPF();
	}
	
	public ArrayList<Atendimento> getAtendimento(){
		return Atendimentos;
	}
	
}
