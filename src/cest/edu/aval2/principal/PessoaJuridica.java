package cest.edu.aval2.principal;

public abstract class PessoaJuridica implements IPessoa {
	public String nome;
	private String cnpj;

	
	@Override
	public String getNome() {
		return getNome();
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	
}
