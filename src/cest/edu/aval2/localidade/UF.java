package cest.edu.aval2.localidade;

public class UF {
	public String sigla;
	public String descricao;
	
	public String getSigla() {
		return sigla;
	}
	public String getDescricao() {
		return descricao;
	}

}
