package cest.edu.aval2.empresa;

import java.math.BigDecimal;

import cest.edu.aval2.principal.PessoaFisica;

public class Funcionario extends PessoaFisica{
	private String cargo;
	private BigDecimal salario;
	
	public String getCargo() {
		return cargo;
	}
	
	public BigDecimal getSalario() {
		return salario;
	}
	
	public Funcionario(String nome,String cargo,BigDecimal salario) {
		
	}

}
