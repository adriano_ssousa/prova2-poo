package cest.edu.aval2.empresa;

import cest.edu.aval2.principal.PessoaJuridica;

public abstract class Fornecedor extends PessoaJuridica{
	String nome;
	String cnpj;
	
	@Override
	public String getNome() {
		return nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public void fornecerMaterial() {
		System.out.println("Material fornecido");
	}
}
